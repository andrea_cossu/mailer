/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.realttech.mailer.pecmailer.impl;

import com.google.common.collect.Lists;
import it.realttech.mailer.pecmailer.Attachment;
import it.realttech.mailer.pecmailer.Exceptions.PecMailerlException;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;
import junit.framework.TestCase;

/**
 *
 * @author acossu
 */
public class PecMailerImplTest extends TestCase {
    
    private String cfgFile; // Percorso al file properties di configurazione
    private String subject; // L'oggetto della mail
    private String txtMessage; // Il corpo della mail (plain text)
    private Properties specProps;
    
    public PecMailerImplTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        cfgFile = "src/test/resources/pecConfTest.properties";
        subject = "";
        txtMessage = "";
        specProps = new Properties();
        specProps.load(new FileInputStream(cfgFile));
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    

    /**
     * Test of sendMail method, of class PecMailerImpl.
     */
    public void testSendMail() {
        System.out.println("sendMail");
        MockPecMailer instance = new MockPecMailer(specProps);
        PecMailerlException pmex = null;
        instance.setSubject("test invio pec con zip");
        instance.setTextMessage("test di invio di una mail pec usando una classe java");
        instance.addRecipient("ing.andreacossu@arubapec.it");
        instance.addCopyRecipient("ing.andreacossu@arubapec.it");
        try {
            Attachment attachPdf = new Attachment("cv.pdf", new File("src/test/resources/cv.pdf") ,"application/pdf");
            Attachment attachTxt = new Attachment("config.properties", new File(cfgFile) ,"text/plain");
            List<Attachment> attachments = Lists.newArrayList();
            attachments.add(attachPdf);
            attachments.add(attachTxt);
            instance.addAttachmentsAsZip(attachments, "allegati.zip");
        
            instance.sendMail();
        } catch (PecMailerlException ex) {
             pmex = ex;
            fail("exeption thrown : " + ex.getMessage());
        }
        
        assertNull(pmex);
    }
    
}
