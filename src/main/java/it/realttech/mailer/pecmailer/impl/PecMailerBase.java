/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.realttech.mailer.pecmailer.impl;

import com.google.common.collect.Lists;
import it.realttech.mailer.pecmailer.IPecMailer;
import it.realttech.mailer.pecmailer.Attachment;
import it.realttech.mailer.pecmailer.Exceptions.PecMailerlException;
import it.realttech.mailer.pecmailer.UserAuthenticator;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@inheritDoc}
 *
 * @author acossu
 */
public class PecMailerBase implements IPecMailer {
        private static final Logger _logger = LoggerFactory.getLogger(PecMailerBase.class);


    //private String cfgFile; // Percorso al file properties di configurazione
    protected String subject; // L'oggetto della mail
    protected String txtMessage; // Il corpo della mail (plain text)
    protected List<String> toAddress; // Elenco dei destinatari (TO)
    protected List<String> ccAddress; // Elenco dei destinatari in copia (CC)
    protected List<Attachment> attachments; // Elenco degli allegati
    protected UserAuthenticator userAuthenticator; // Autenticatore per SMTP
    protected Properties specProps; // properties di configurazione
    protected String messageEncoding; // encoding del messaggio - default utf-8



    /**
     * {@inheritDoc}
     */
    @Override
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addRecipient(String recipient) {
        toAddress.add(recipient);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void addRecipients(List<String> recipients) {
        toAddress.addAll(recipients);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addCopyRecipient(String ccRec) {
        ccAddress.add(ccRec);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void addCopyRecipients(List<String> ccRecipients) {
        ccAddress.addAll(ccRecipients);
    }    
    

    /**
     * {@inheritDoc}
     */
    @Override
    public void addAttachment(Attachment attachment, String newName) {
        attachments.add(attachment);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void addAttachments(List<Attachment> attachments) {
        attachments.addAll(attachments);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void addAttachmentsAsZip(List<Attachment> attachments, String zipFileName) throws PecMailerlException{
        InputStream is = this.createZipStream(attachments, zipFileName);
        Attachment zip = new Attachment(zipFileName, is, "application/octet-stream");
        this.attachments.add(zip);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setTextMessage(String txtMsg) {
        this.txtMessage = txtMsg;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void sendMail() throws PecMailerlException {
        // implemented in derived classes
    }    

    /*
     * Si occupa di comporre il messaggio: costruisce il corpo del messaggio
     * (testo) e aggiunge tutti gli eventuali allegati
     */
    protected void componiMessaggio(MimeMessage message) throws Exception {
        Multipart multipart = new MimeMultipart();

        // Creo il corpo del messaggio con il testo
        BodyPart bodyPart = new MimeBodyPart();
        ((MimeBodyPart) bodyPart).setText(txtMessage, "UTF-8");

        multipart.addBodyPart(bodyPart);

        // Se ci sono allegati, li allego
        for (Attachment al : attachments) {        
            bodyPart = new MimeBodyPart();
            ByteArrayDataSource ds = new ByteArrayDataSource(al.getAttachmentFile(), al.getContentType());
            bodyPart.setDataHandler(new DataHandler(ds));
            bodyPart.setFileName(al.getAttachmentName());
            bodyPart.setDisposition(Part.ATTACHMENT);

            multipart.addBodyPart(bodyPart);
        }

        // Aggiungo il Multipart al messaggio
        message.setContent(multipart);
    }
    
    protected InputStream createZipStream(List<Attachment> attachments, String zipFileName) throws PecMailerlException{
        File zipFile = null;
        BufferedInputStream zipFileInputStream = null;
        try {
            zipFile = File.createTempFile(zipFileName, ".tmp");
            zipFile.deleteOnExit();
            ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFile)));
            for (Attachment attachment : attachments) {
                ZipEntry ze = new ZipEntry(attachment.getAttachmentName());
                zos.putNextEntry(ze);
                byte[] bytes = new byte[1024];
                InputStream is = attachment.getAttachmentFile();
                int count = is.read(bytes);
                while (count > -1){
                    zos.write(bytes, 0, count);
                    count = is.read(bytes);
                }
                is.close();
                zos.closeEntry();
            }
            zos.close();
            zipFileInputStream = new BufferedInputStream(new FileInputStream(zipFile));
        } catch (IOException ex) {
            StringBuilder message = new StringBuilder("Errore nella creazione dello zip: ");
            message.append(" - ");
            message.append("stackTrace: " + Arrays.toString(ex.getStackTrace()));   
            _logger.error(message.toString());
            throw new PecMailerlException(message.toString());
        }
        
        return zipFileInputStream;
    }

}
