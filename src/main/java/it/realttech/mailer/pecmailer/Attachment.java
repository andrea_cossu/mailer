/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.realttech.mailer.pecmailer;

import it.realttech.mailer.pecmailer.Exceptions.PecMailerlException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/*
 * Attachment: rappresenta un singolo file da allegare alla mail.
 * Il singolo allegato &egrave; rappresentato da un file fisico
 * e da un nome da visualizzare all'interno della mail.
 *
 * @author acossu
 */
public class Attachment {

    private String attachmentName;
    //private File attachmentFile;
    private InputStream attachmentFile;
    private String contentType;

    public Attachment(String nomeAllegato, File fileAllegato, String contentType) throws PecMailerlException{
        this.attachmentName = nomeAllegato;
        //this.attachmentFile = fileAllegato;
        this.contentType = contentType;
        this.attachmentFile = getStreamFromFile(nomeAllegato, fileAllegato);
    }
    
    public Attachment(String nomeAllegato, InputStream fileAllegato, String contentType) throws PecMailerlException{
        this.attachmentName = nomeAllegato;
        //this.attachmentFile = this.getFileFromStream(nomeAllegato, fileAllegato);
        this.contentType = contentType;
        this.attachmentFile = fileAllegato;
    }
    
    public Attachment(String nomeAllegato, byte[] fileAllegato, String contentType) throws PecMailerlException{
        this.attachmentName = nomeAllegato;
        this.contentType = contentType;
        this.attachmentFile = new ByteArrayInputStream(fileAllegato);
    }

    public String getAttachmentName() {
        return attachmentName;
    }

   /* public File getAttachmentFile() {
        return attachmentFile;
    }*/
    
    public InputStream getAttachmentFile() {
        return attachmentFile;
    }    
    
    public String getContentType() {
        return contentType;
    }     
    
    
 /*   private File getFileFromStream(String nomeAllegato, InputStream fileAllegato) throws PecMailerlException{
        File file = null;
        try {
            file = File.createTempFile(nomeAllegato, ".tmp");
            file.deleteOnExit();
        } catch (IOException ex) {
            StringBuilder message = new StringBuilder("Errore durante la creazione dell'allegato : " + nomeAllegato);
            message.append(" - ");
            message.append("stackTrace: " + ex.getStackTrace());               
            throw new PecMailerlException(message.toString());
        }
        
        return file;
    }
    
    
    private InputStream getStreamFromByteArray(byte[] file){
        InputStream is = null;
        is = new ByteArrayInputStream(file);
        return is;
    }
    */
    private InputStream getStreamFromFile(String nomeAllegato, File file) throws PecMailerlException{
        InputStream is = null;
        try {
            is = new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            StringBuilder message = new StringBuilder("Errore durante la lettura del file : " + nomeAllegato);
            message.append(" - ");
            message.append("stackTrace: " + ex.getStackTrace());               
            throw new PecMailerlException(message.toString());
        }
         
        return is;
    }

}
