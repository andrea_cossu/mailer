/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.realttech.mailer.pecmailer;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * UserAuthenticator: fornise un'implementazione concreta di Authenticator;
 * permette, quindi, di fornire all'applicazione (in chiaro) l'utente e la
 * password per l'accesso al server SMTP
 *
 * @author acossu
 */
public class UserAuthenticator extends Authenticator {

    private String user;
    private String pass;

    public UserAuthenticator(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(user, pass);
    }

}
