package it.realttech.mailer.pecmailer;

import it.realttech.mailer.pecmailer.Exceptions.PecMailerlException;
import java.util.List;

/**
 * This class allow sending email using a PEC (Certified Eletronic Mail)
 *
 * It manages the whole comunication with SMTP server using SSL protocol and the
 * JavaMail standard library.
 *
 * Attachements are allowed.
 * 
 * @author acossu
 */
public interface IPecMailer {
    
    /**
     * Imposta l'oggetto della mail
     *
     * @param subject l'oggetto della mail
     */    
    void setSubject(String subject);

    /**
     * Aggiunge un destinatario all'elenco dei destinatari di questa mail
     *
     * @param recipient l'indirizzo email del destinatario da aggiungere
     */    
    void addRecipient(String recipient);
    
    /**
     * Aggiunge una lista di destinatari all'elenco dei destinatari di questa mail
     *
     * @param recipients la lista degli indirizzi email dei destinatari da aggiungere
     */ 
    void addRecipients(List<String> recipients);
    
    /**
     * Aggiunge un destinatario all'elenco dei destinatari in copia
     *
     * @param ccRec l'indirizzo email del destinatario in copia da aggiungere
     */    
    void addCopyRecipient(String ccRec);
    
    /**
     * Aggiunge una lista di destinatari all'elenco dei destinatari in copia di questa mail
     *
     * @param ccRecipients la lista degli indirizzi email dei destinatari in copia da aggiungere
     */ 
    void addCopyRecipients(List<String> ccRecipients);    
    
    /**
     * Aggiunge un allegato alla mail
     *
     * @param attachment un oggetto Attachment che rappresenta il file da allegare
     * @param newName il nome dell'allegato da visualizzare nella mail
     */
    void addAttachment(Attachment attachment, String newName);
    
    /**
     * Aggiunge un lista di allegati alla mail
     *
     * @param attachments una lista di allegati da allegare alla mail
     */
    void addAttachments(List<Attachment> attachments);
    
    /**
     * Aggiunge un lista di allegati alla mail come file zip
     * 
     * @param attachments una lista di allegati da inserire nel file zip
     * @throws PecMailerlException 
     */
    void addAttachmentsAsZip(List<Attachment> attachments, String zipFileName) throws PecMailerlException;
    
    /**
     * Imposta il contenuto testuale della mail (corpo del messaggio)
     *
     * @param txtMsg array di stringhe: ciascun elemento rappresenta una riga
     * del corpo messaggio
     */    
    void setTextMessage(String txtMsg);
    
    /**
     * Effettua l'invio della mail
     * 
     * @throws it.realttech.mailer.pecmailer.Exceptions.PecMailerlException
     */    
    void sendMail() throws PecMailerlException;
}
