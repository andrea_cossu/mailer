/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.realttech.mailer.pecmailer.impl;

import com.google.common.collect.Lists;
import it.realttech.mailer.pecmailer.Attachment;
import it.realttech.mailer.pecmailer.Exceptions.PecMailerlException;
import it.realttech.mailer.pecmailer.UserAuthenticator;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author acossu
 */
public class PecMailerImpl extends PecMailerBase {
    private static final Logger _logger = LoggerFactory.getLogger(PecMailerImpl.class);

    /**
     * Costruttore 
     * Le properties richieste sono:
     * mail.smtp.host=server_smtp_della_pec 
     * mail.smtp.socketFactory.port=465
     * mail.smtp.socketFactory.class=javax.net.ssl.SSLSocketFactory
     * mail.smtp.auth=true mail.smtp.port=465 mail.mime.charset=UTF-8
     * nomeutente=nome_utente_da_usare_per_autenticazione
     * password=password_da_usare_per_autenticazione
     * from-address=indirizzo_email_del_mittente
     * from-name=nome_da_visualizzare_nel_campo_del_mittente
     *
     * @param prop properties di configurazione.
     */
    public PecMailerImpl(Properties prop) {
        this.specProps = prop;
        this.subject = "";
        this.txtMessage = "";
        this.toAddress = Lists.<String>newArrayList();
        this.ccAddress = Lists.<String>newArrayList();
        this.attachments = Lists.<Attachment>newArrayList();
        this.messageEncoding = "UTF-8";
    }

    /**
     * Costruttore
     * 
     * @param prop properties di configurazione.
     * @param subject il subject della mail
     * @param txtMessage il testo della mail
     * @param toAddress lista di indirizzi dei destinatari
     * @param ccAddress lista di indirizzi dei destinatari in copia
     * @param attachments lista degli allegati
     * @param messageEncoding encoding del messaggio
     */
    public PecMailerImpl(Properties prop, String subject, String txtMessage, List<String> toAddress, List<String> ccAddress, List<Attachment> attachments, String messageEncoding) {
        this.specProps = prop;
        this.subject = subject;
        this.txtMessage = txtMessage;
        this.toAddress = Lists.<String>newArrayList(toAddress);
        this.ccAddress = Lists.<String>newArrayList(ccAddress);
        this.attachments = Lists.<Attachment>newArrayList(attachments);
        this.messageEncoding = messageEncoding;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendMail() throws PecMailerlException {
        try {
            // Ottengo le proprietà  di sistema
            Properties props = System.getProperties();

            // Aggiungo alle proprietà  specifiche, tutte le proprietà generali di sistema
            Set<String> keys = props.stringPropertyNames();
            for (String key : keys) {
                specProps.put(key, props.getProperty(key));
            }

            // Ottengo le informazioni per l'autenticazione e l'invio della mail
            String user = specProps.getProperty("nomeutente");
            String pass = specProps.getProperty("password");
            String fromAddress = specProps.getProperty("from-address");
            String personalName = specProps.getProperty("from-name");

            // Rimuovo le proprietà  appena lette dalle proprietà  specifiche
            specProps.remove("nomeutente");
            specProps.remove("password");
            specProps.remove("from-address");
            specProps.remove("from-name");

            // La porta da usare per la connessione al server SMTP
            int port = Integer.parseInt(specProps.getProperty("mail.smtp.port"));

            // Costruisco l'oggetto authenticator per effettuare l'autenticazione al server SMTP
            userAuthenticator = new UserAuthenticator(user, pass);

            // Ottengo una sessione SMTP
            Session session = Session.getDefaultInstance(specProps, userAuthenticator);

            // Costruisco l'oggeto mail
            MimeMessage message = new MimeMessage(session);

            // Imposto il mittente
            InternetAddress iaFrom = new InternetAddress(fromAddress, personalName);
            message.setFrom(iaFrom);

            // Aggiungo i destinatari principali
            for (String rec : toAddress) {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(rec));
            }

            // Aggiungo i destinatari in copia
            for (String rec : ccAddress) {
                message.addRecipient(Message.RecipientType.CC, new InternetAddress(rec));
            }

            // Setto l'oggetto della mail
            message.setSubject(subject);

            // Creo il corpo del messaggio (il testo + gli allegati)
            componiMessaggio(message);

            // Salvo lo stato dell'oggetto mail
            message.saveChanges();

            // Dalla sessione SMTP ottengo il Transport che si occuperà di dialogare col server
            Transport t = session.getTransport("smtps");

            // Effettuo la connessione
            t.connect(specProps.getProperty("mail.smtp.host"), port, user, pass);

            // Invio il messaggio
            t.sendMessage(message, message.getAllRecipients());

            // Chiudo la connessione
            t.close();

        } catch (AddressException aex) {
            StringBuilder message = new StringBuilder("Indirizzo che ha generato l'eccezione: " + aex.getRef());
            message.append(" - ");
            message.append("Posizione occorrenza errore: " + aex.getPos());
            message.append(" - ");
            message.append("stackTrace: " + Arrays.toString(aex.getStackTrace()));
            message.append("\n\r");
            message.append("caused by: " + Arrays.toString(aex.getCause().getStackTrace()));
            _logger.error(message.toString());
            throw new PecMailerlException(message.toString(), aex);
        } catch (Exception ex) {
            StringBuilder message = new StringBuilder("Errore generico: ");
            message.append(" - ");
            message.append("stackTrace: " + Arrays.toString(ex.getStackTrace()));
            message.append("\n\r");
            message.append("caused by: " + Arrays.toString(ex.getCause().getStackTrace()));            
            _logger.error(message.toString());
            throw new PecMailerlException(message.toString());
        }
    }

}
