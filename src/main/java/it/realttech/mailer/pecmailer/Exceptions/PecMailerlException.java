/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.realttech.mailer.pecmailer.Exceptions;

/**
 *
 * @author acossu
 */
public class PecMailerlException extends Exception {

    private static final long serialVersionUID = 1997753363232807009L;

    public PecMailerlException() {

    }

    public PecMailerlException(String message) {
        super(message);
    }

    public PecMailerlException(Throwable cause) {
        super(cause);
    }

    public PecMailerlException(String message, Throwable cause) {
        super(message, cause);

    }

    public PecMailerlException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);

    }
}
