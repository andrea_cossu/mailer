/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.realttech.mailer.pecmailer.impl;

import com.google.common.collect.Lists;
import it.realttech.mailer.pecmailer.Attachment;
import it.realttech.mailer.pecmailer.Exceptions.PecMailerlException;
import it.realttech.mailer.pecmailer.UserAuthenticator;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Esegue tutte le operazioni di creazione della mail 
 * ma non effettua la connessione al server e l'invio
 * 
 *
 * @author acossu
 */
public class MockPecMailer extends PecMailerBase {
    private static final Logger _logger = LoggerFactory.getLogger(MockPecMailer.class);    
    
    public MockPecMailer(Properties prop){
        this.specProps = prop;
        this.subject = "";
        this.txtMessage = "";
        this.toAddress = Lists.<String>newArrayList();
        this.ccAddress = Lists.<String>newArrayList();
        this.attachments = Lists.<Attachment>newArrayList();
        this.messageEncoding = "UTF-8";
    }
    
    public MockPecMailer(Properties prop, String subject, String txtMessage, List<String> toAddress, List<String> ccAddress, List<Attachment> attachments, String messageEncoding){
        this.specProps = prop;
        this.subject = subject;
        this.txtMessage = txtMessage;
        this.toAddress = Lists.<String>newArrayList(toAddress);
        this.ccAddress = Lists.<String>newArrayList(ccAddress);
        this.attachments = Lists.<Attachment>newArrayList(attachments);
        this.messageEncoding = messageEncoding;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void sendMail() throws PecMailerlException{
        try {
            // Ottengo le proprietà  di sistema
            Properties props = System.getProperties();

            // Aggiungo alle proprietà  specifiche, tutte le proprietà generali di sistema
            Set<String> keys = props.stringPropertyNames();
            for (String key : keys) {
                specProps.put(key, props.getProperty(key));
            }

            // Ottengo le informazioni per l'autenticazione e l'invio della mail
            String user = specProps.getProperty("nomeutente");
            String pass = specProps.getProperty("password");
            String fromAddress = specProps.getProperty("from-address");
            String personalName = specProps.getProperty("from-name");

            // Rimuovo le proprietà  appena lette dalle proprietà  specifiche
            specProps.remove("nomeutente");
            specProps.remove("password");
            specProps.remove("from-address");
            specProps.remove("from-name");

            // La porta da usare per la connessione al server SMTP
            int port = Integer.parseInt(specProps.getProperty("mail.smtp.port"));

            // Costruisco l'oggetto authenticator per effettuare l'autenticazione al server SMTP
            userAuthenticator = new UserAuthenticator(user, pass);

            // Ottengo una sessione SMTP
            Session session = Session.getDefaultInstance(specProps, userAuthenticator);

            // Costruisco l'oggeto mail
            MimeMessage message = new MimeMessage(session);

            // Imposto il mittente
            
            InternetAddress iaFrom = new InternetAddress(fromAddress, personalName);
            message.setFrom(iaFrom);
            _logger.info("Impostato il mittente: " + fromAddress);

            // Aggiungo i destinatari principali
            _logger.info("Elenco destinatari da aggiungere: " + Arrays.toString(toAddress.toArray()));
            for (String rec : toAddress) {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(rec));
                _logger.info("Aggiunto il destinatario: " + rec);
            }
            _logger.info("Aggiunti " + toAddress.size() + " destinatari");
            

            // Aggiungo i destinatari in copia
            _logger.info("Elenco destinatari in CC da aggiungere: " + Arrays.toString(ccAddress.toArray()));
            for (String rec : ccAddress) {
                message.addRecipient(Message.RecipientType.CC, new InternetAddress(rec));
                _logger.info("Aggiunto il destinatario in cc: " + rec);
            }
            _logger.info("Aggiunti " + ccAddress.size() + " destinatari in copia.");  

            // Setto l'oggetto della mail
            message.setSubject(subject);
            _logger.info("Ho settato il soggetto della mail");

            // Creo il corpo del messaggio (il testo + gli allegati)
            componiMessaggio(message);
            _logger.info("ho finito di comporre il messaggio (il testo + gli allegati)");

            // Salvo lo stato dell'oggetto mail
            message.saveChanges();
            _logger.info("ho salvato lo stato dell'oggetto mail");

            // Dalla sessione SMTP ottengo il Transport che si occuperà di dialogare col server
            Transport t = session.getTransport("smtps");
            _logger.info("ho aperto il canale smtps");

            // Effettuo la connessione
            _logger.info("connessione al server di posta (Mock)");

            // Invio il messaggio
            _logger.info("invio del messaggio (Mock)");

            // Chiudo la connessione
            t.close();
            _logger.info("ho chiuso il canale smtps");

        } catch (AddressException aex) {
            StringBuilder message = new StringBuilder("Indirizzo che ha generato l'eccezione: " + aex.getRef());
            message.append(" - ");
            message.append("Posizione occorrenza errore: " + aex.getPos());
            message.append(" - ");
            message.append("stackTrace: " + Arrays.toString(aex.getStackTrace()));
            _logger.error(message.toString());
            throw new PecMailerlException(message.toString(), aex);
        } catch (Exception ex) {
            StringBuilder message = new StringBuilder("Errore generico: ");
            message.append(" - ");
            message.append("stackTrace: " + Arrays.toString(ex.getStackTrace()));  
            _logger.error(message.toString());
            throw new PecMailerlException(message.toString());
        } 
    }    
    
    /**
     * {@inheritDoc}
     */    
    @Override
    protected void componiMessaggio(MimeMessage message) throws PecMailerlException{
        try {
            Multipart multipart = new MimeMultipart();
            
            // Creo il corpo del messaggio con il testo
            BodyPart bodyPart = new MimeBodyPart();
            ((MimeBodyPart) bodyPart).setText(txtMessage, "UTF-8");
            
            multipart.addBodyPart(bodyPart);
            _logger.info("Ho aggiunto il testo al corpo del messaggio");
            
            // Se ci sono allegati, li allego
            _logger.info("Sto tentando di allegare " + attachments.size() + " files alla mail");
            for (Attachment al : attachments) {
                _logger.info("Sto tentando di allegare il seguente file alla mail: " + al.getAttachmentName());
                bodyPart = new MimeBodyPart();
                ByteArrayDataSource ds = new ByteArrayDataSource(al.getAttachmentFile(), al.getContentType());
                bodyPart.setDataHandler(new DataHandler(ds));
                bodyPart.setFileName(al.getAttachmentName());
                bodyPart.setDisposition(Part.ATTACHMENT);
                
                multipart.addBodyPart(bodyPart);
                _logger.info("Ho aggiunto il seguente allegato alla mail: " );
            }
            
            // Aggiungo il Multipart al messaggio
            message.setContent(multipart);
            _logger.info("Ho aggiunto " + (multipart.getCount()-1) + " allegati alla mail");
        } catch (Exception ex) {
            StringBuilder msg = new StringBuilder("Errore nella creazione del messaggio: ");
            msg.append(" - ");
            msg.append("stackTrace: " + Arrays.toString(ex.getStackTrace()));   
            _logger.error(msg.toString());
            throw new PecMailerlException(msg.toString());
        }

    }
    
    /**
     * {@inheritDoc}
     */    
    @Override    
    protected InputStream createZipStream(List<Attachment> attachments, String zipFileName) throws PecMailerlException{
        File zipFile = null;
        BufferedInputStream zipFileInputStream = null;
        try {
            zipFile = File.createTempFile(zipFileName, ".tmp",new File("C:\\lavoro\\tools\\apache-tomcat-7.0.64\\temp"));
            zipFile.deleteOnExit();
            ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFile)));
            for (Attachment attachment : attachments) {
                ZipEntry ze = new ZipEntry(attachment.getAttachmentName());
                zos.putNextEntry(ze);
                byte[] bytes = new byte[1024];
                InputStream is = attachment.getAttachmentFile();
                int count = is.read(bytes);
                while (count > -1){
                    zos.write(bytes, 0, count);
                    count = is.read(bytes);
                }
                is.close();
                zos.closeEntry();
                _logger.info("Ho aggiunto il file: " + attachment.getAttachmentName() + " allo zip");
            }
            zos.close();
            zipFileInputStream = new BufferedInputStream(new FileInputStream(zipFile));
            _logger.info("Ho creato il file zip: " + zipFileName);
        } catch (IOException ex) {
            StringBuilder message = new StringBuilder("Errore nella creazione dello zip: ");
            message.append(" - ");
            message.append("stackTrace: " + Arrays.toString(ex.getStackTrace()));   
            _logger.error(message.toString());
            throw new PecMailerlException(message.toString());
        }
        
        return zipFileInputStream;
    }    
}
